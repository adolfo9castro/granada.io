# AngularApp Granadaio
Este repositorio ha sido creado para almacenar el código para la prueba de desarrollador en la empresa Granada.io

Este proyecto fue desarrollador con  [Angular CLI](https://github.com/angular/angular-cli)

## Ejecución de la aplicación

Para ejecutar la aplicación desde consola: `ng serve` se visualizará en la ruta: `http://localhost:4200/`.

## Ejecución de pruebas unitarias

Ejecutar desde consola: `ng test`.
[Karma](https://karma-runner.github.io).

##Nota
La aplicación fue ejecutada con éxito desde ubuntu 17.04, aun no tiene estilos pero tiene lo que pedía la prueba.

- Cualquier decisión con respecto al cargo agradecería que me escribieran al correo: [castro09@gmail.com](mailto:castro09@gmail.com)
