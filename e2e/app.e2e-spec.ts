import { GranadaioPage } from './app.po';

describe('granadaio App', function() {
  let page: GranadaioPage;

  beforeEach(() => {
    page = new GranadaioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
