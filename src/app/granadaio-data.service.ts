import { Injectable } from '@angular/core';
import { Granadaio } from './granadaio';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GranadaioDataService {
     constructor(private api: ApiService) {}

     // Simulate POST /items
     addItem(item: Item): Observable<Granadaio> {
          return this.api.createTodo(item);
     }

     // Simulate DELETE /items/:id
     deleteItemById(itemId: number): Observable<Granadaio> {
          return this.api.deleteItemById(itemId);
     }

     // Simulate PUT /items/:id
     updateItem(item: Granadaio): Observable<Granadaio> {
          return this.api.updateItem(item);
     }

     // Simulate GET /items
     getAllItems(): Observable<Granadaio[]> {
          return this.api.getAllItems();
     }

     // Simulate GET /items/:id
     getItemById(itemId: number): Observable<Granadaio> {
          return this.api.getItemById(itemId);
     }

     // Toggle complete
     toggleItemComplete(item: Granadaio) {
          item.complete = !item.complete;
          return this.api.updateItem(item);
     }

}
