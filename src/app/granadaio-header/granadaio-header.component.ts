import { Component, Output, EventEmitter } from '@angular/core';
import { Granadaio } from '../granadaio';

@Component({
     selector: 'app-granadaio-header',
     templateUrl: './granadaio-header.component.html',
     styleUrls: ['./granadaio-header.component.css']
})
export class GranadaioHeaderComponent{

     newItem: Granadaio = new Granadaio();

     @Output()
     add: EventEmitter<Granadaio> = new EventEmitter();

     constructor() {}

     addItem() {
          this.add.emit(this.newItem);
          this.newItem = new Granadaio();
     }
}
