import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Http, Response } from '@angular/http';
import { Granadaio } from './granadaio';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

     constructor(private http: Http){}

     public getAllItemss():Observable<Granadaio[]> {
          return this.http.get(API_URL + '/items').map(response => {
               const items = response.json();
               return items.map((item) => new Granadaio(item));
         }).catch(this.handleError)
     }

     public createItems(item: Granadaio): Observable<Granadaio> {
          return this.http.post(API_URL + '/items', item).map(response => {
               return new Granadaio(response.json());
          }).catch(this.handleError);
     }

     public getItemsById(itemId: number): Observable<Granadaio> {
          return this.http.get(API_URL + '/items/' + itemId).map(response => {
               return new Granadaio(response.json());
          }).catch(this.handleError);
     }

     public updateItems(item: Granadaio): Observable<Granadaio> {
          return this.http.put(API_URL + '/items/' + item.id, item).map(response => {
               return new Granadaio(response.json());
          }).catch(this.handleError);
     }

     public deleteItemsById(itemId: number): Observable<null> {
          return this.http.delete(API_URL + '/items/' + itemId).map(response => null).catch(this.handleError);
     }

}
