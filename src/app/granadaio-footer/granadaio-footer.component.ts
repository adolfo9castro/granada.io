import { Component, Input } from '@angular/core';
import { Granadaio } from '../granadaio';

@Component({
     selector: 'app-granadaio-footer',
     templateUrl: './granadaio-footer.component.html',
     styleUrls: ['./granadaio-footer.component.css']
})
export class GranadaioFooterComponent{
     @Input()
     item: Granadaio[];

     constructor() { }
}
