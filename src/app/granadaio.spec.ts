import {Granadaio} from './granadaio';

describe('Granadaio', () => {
     it('should create an instance', () => {
          expect(new Granadaio()).toBeTruthy();
     });
});

it('should accept values in the constructor', () => {
     let todo = new Todo({
          title: 'hello',
          complete: true
     });
     expect(todo.title).toEqual('hello');
     expect(todo.complete).toEqual(true);
});
