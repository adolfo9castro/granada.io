import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GranadaioHeaderComponent } from './granadaio-header/granadaio-header.component';
import { GranadaioComponent } from './granadaio/granadaio.component';
import { GranadaioBodyComponent } from './granadaio-body/granadaio-body.component';
import { GranadaioItemComponent } from './granadaio-item/granadaio-item.component';
import { GranadaioFooterComponent } from './granadaio-footer/granadaio-footer.component';
import { ApiService } from './api.service';

@NgModule({
  declarations: [
    AppComponent,
    GranadaioHeaderComponent,
    GranadaioComponent,
    GranadaioItemComponent,
    GranadaioFooterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [GranadaioDataService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
