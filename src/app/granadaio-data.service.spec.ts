import { TestBed, async, inject } from '@angular/core/testing';
import { Granadaio } from './granadaio';
import { GranadaioDataService } from './granadaio-data.service';

describe('GranadaioDataService', () => {
     beforeEach(() => {
          TestBed.configureTestingModule({
               providers: [GranadaioDataService]
          });
     });

     it('should ...', inject([GranadaioDataService], (service: GranadaioDataService) => {
          expect(service).toBeTruthy();
     }));

     describe('#getAllItems()', () => {
          it('should return an empty array by default', inject([ItemDataService], (service: ItemDataService) => {
               expect(service.getAllItems()).toEqual([]);
          }));

          it('should return all items', inject([ItemDataService], (service: ItemDataService) => {
               let item1 = new Granadaio({title: 'Hello 1', complete: false});
               let item2 = new Granadaio({title: 'Hello 2', complete: true});
               service.addItem(item1);
               service.addItem(item2);
               expect(service.getAllItems()).toEqual([item1, item2]);
          }));

     });

     describe('#save(item)', () => {

          it('should automatically assign an incrementing id', inject([ItemDataService], (service: ItemDataService) => {
               let item1 = new Granadaio({title: 'Hello 1', complete: false});
               let item2 = new Granadaio({title: 'Hello 2', complete: true});
               service.addItem(item1);
               service.addItem(item2);
               expect(service.getItemById(1)).toEqual(item1);
               expect(service.getItemById(2)).toEqual(item2);
          }));

     });

     describe('#deleteItemById(id)', () => {

          it('should remove item with the corresponding id', inject([ItemDataService], (service: ItemDataService) => {
               let item1 = new Granadaio({title: 'Hello 1', complete: false});
               let item2 = new Granadaio({title: 'Hello 2', complete: true});
               service.addItem(item1);
               service.addItem(item2);
               expect(service.getAllItems()).toEqual([item1, item2]);
               service.deleteItemById(1);
               expect(service.getAllItems()).toEqual([item2]);
               service.deleteItemById(2);
               expect(service.getAllItems()).toEqual([]);
          }));

          it('should not removing anything if item with corresponding id is not found', inject([ItemDataService], (service: ItemDataService) => {
               let item1 = new Granadaio({title: 'Hello 1', complete: false});
               let item2 = new Granadaio({title: 'Hello 2', complete: true});
               service.addItem(item1);
               service.addItem(item2);
               expect(service.getAllItems()).toEqual([item1, item2]);
               service.deleteItemById(3);
               expect(service.getAllItems()).toEqual([item1, item2]);
          }));

     });

     describe('#updateItemById(id, values)', () => {

          it('should return item with the corresponding id and updated data', inject([ItemDataService], (service: ItemDataService) => {
               let item = new Granadaio({
                    title: 'Hello 1', complete: false
               });
               service.addItem(item);
               let updatedItem = service.updateItemById(1, {
                    title: 'new title'
               });
               expect(updatedItem.title).toEqual('new title');
          }));

          it('should return null if item is not found', inject([ItemDataService], (service: ItemDataService) => {
               let item = new Granadaio({
                    title: 'Hello 1', complete: false
               });
               service.addItem(item);
               let updatedItem = service.updateItemById(2, {
                    title: 'new title'
               });
               expect(updatedItem).toEqual(null);
          }));

     });

     describe('#toggleItemComplete(item)', () => {

          it('should return the updated item with inverse complete status', inject([ItemDataService], (service: ItemDataService) => {
               let item = new Granadaio({
                    title: 'Hello 1', complete: false
               });
               service.addItem(item);
               let updatedItem = service.toggleItemComplete(item);
               expect(updatedItem.complete).toEqual(true);
               service.toggleItemComplete(item);
               expect(updatedItem.complete).toEqual(false);
          }));
     });
});
