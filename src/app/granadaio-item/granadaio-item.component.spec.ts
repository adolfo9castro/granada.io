/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GranadaioItemComponent } from './granadaio-item.component';

describe('GranadaioItemComponent', () => {
  let component: GranadaioItemComponent;
  let fixture: ComponentFixture<GranadaioItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GranadaioItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GranadaioItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
