import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Granadaio } from '../granadaio';

@Component({
  selector: 'app-granadaio-item',
  templateUrl: './granadaio-item.component.html',
  styleUrls: ['./granadaio-item.component.css']
})
export class GranadaioItemComponent{
     @Input() item: Granadaio[];

     @Output()
     remove: EventEmitter<Granadaio> = new EventEmitter();

     @Output()
     toggleComplete: EventEmitter<Granadaio> = new EventEmitter();

     constructor() {
     }

     toggleItemComplete(item: Granadaio) {
     this.toggleComplete.emit(item);
     }

     removeItem(item: Granadaio) {
     this.remove.emit(item);
     }
}
