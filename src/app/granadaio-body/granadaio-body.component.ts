import { Component,EventEmitter, Input, Output } from '@angular/core';
import { Granadaio } from '../granadaio';

@Component({
     selector: 'app-granadaio-body',
     templateUrl: './granadaio-body.component.html',
     styleUrls: ['./granadaio-body.component.css']
})
export class GranadaioBodyComponent{

     @Input()
     items: Granadaio[];

     @Output()
     remove: EventEmitter<Granadaio> = new EventEmitter();

     @Output()
     toggleComplete: EventEmitter<Granadaio> = new EventEmitter();

     constructor() {
     }

     onToggleItemComplete(item: Granadaio) {
          this.granadaioDataService.toggleItemComplete(item);
     }

     onRemoveItem(item: Granadaio) {
          this.granadaioDataService.deleteItemById(item.id);
     }

}
