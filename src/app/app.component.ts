import { Component, OnInit } from '@angular/core';
import { GranadaioDataService } from './granadaio-data.service';
import { Granadaio } from './granadaio';

@Component({
     selector: 'app-root',
     templateUrl: './app.component.html',
     styleUrls: ['./app.component.css'],
     providers: [GranadaioDataService]
})
export class AppComponent implements OnIni{
     items: Granadaio[]= [];

     constructor(private granadaioDataService: GranadaioDataService) {
     }

     public ngOnInit() {
          this.itemDataService.getAllitems().subscribe(
               (items) => {
                    this.items = items;
               }
          );
     }

     onAddItem(item) {
          this.itemDataService.addItem(item).subscribe(
               (newItem) => {
                    this.items = this.items.concat(newItem);
               }
          );
     }

     onToggleItemComplete(item) {
          this.itemDataService.toggleItemComplete(item).subscribe(
               (updatedItem) => {
                    item = updatedItem;
               }
          );
     }

     onRemoveItem(item) {
          this.itemDataService.deleteItemById(item.id).subscribe(
               (_) => {
                    this.items = this.items.filter((t) => t.id !== item.id);
               }
          );
     }

}
